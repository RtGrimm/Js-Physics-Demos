function Vec2(x, y) {
  this.X = x;
  this.Y = y;

  this.add = (other) => {
    return new Vec2(this.X + other.X, this.Y + other.Y);
  };

  this.subtract = (other) => {
    return new Vec2(this.X - other.X, this.Y - other.Y);
  };

  this.multiply = (other) => {
    return new Vec2(this.X * other, this.Y * other);
  };

  this.length = () => {
    return Math.sqrt(this.X * this.X + this.Y * this.Y);
  };

  this.unit = () => {
    let length = this.length();
    return new Vec2(this.X / length, this.Y / length);
  };

  this.abs = () => {
    return new Vec2(Math.abs(this.X), Math.abs(this.Y));
  };

  this.rand = () => {
    return new Vec2(Math.random(), Math.random());
  };
}



function App(updateCallback, drawCallback, renderTargetId) {
  this.lastTime = Date.now();
  this.mousePos = new Vec2(0, 0);
  this.mouseDown = false;
  this.deltaTime = 0;

  this.renderTarget = document.querySelector(renderTargetId);
  this.context = this.renderTarget.getContext("2d");

  this.renderTarget.addEventListener("mousemove", (e) => {
    this.mousePos = new Vec2(e.clientX, e.clientY);
  });

  this.renderTarget.addEventListener("mousedown", (e) => {
    this.mouseDown = true;
  });

  this.renderTarget.addEventListener("mouseup", (e) => {
    this.mouseDown = false;
  });

  this.bounds = new Vec2(1600, 1200);

  this.run = function() {
    window.requestAnimationFrame(() => {
      this.deltaTime = (Date.now() - this.lastTime) / 1000;
      this.context.clearRect(0, 0, this.bounds.X, this.bounds.Y);


      updateCallback(this.deltaTime);
      drawCallback(this.context);

      this.run();

      this.lastTime = Date.now();
    });
  }
}

let particles = [

];


let maxParticleCount = 50;
let removeMargin = 100;
let lineDistance = 200;
let friction = 3;


let app = new App(
  update, draw, "#RenderTarget");


function getForces(targetBody) {
  return [];
}

function spawnParticle(spawnOutside) {
  let fixedOffset = 10;
  let randomOffset = Math.random() * 1000;

  let side = Math.round(Math.random() * 4);

  let spawnLocation = new Vec2(-fixedOffset, -fixedOffset);

  if(spawnOutside) {
    if(side == 0) {
      spawnLocation = new Vec2(-fixedOffset, randomOffset);
    } else if(side == 1) {
      spawnLocation = new Vec2(randomOffset, -fixedOffset);
    } else if(side == 2) {
      spawnLocation = new Vec2(app.bounds.X + fixedOffset, randomOffset);
    } else if(side == 3) {
      spawnLocation = new Vec2(randomOffset, app.bounds.X + fixedOffset);
    }
  } else {
    spawnLocation = new Vec2().rand().multiply(1000);
  }

  let constantForce = new Vec2(0, 0).rand()
    .add(new Vec2(-0.5, -0.5)).multiply(1000);

  particles.push({
    location : spawnLocation,
    velocity : new Vec2(0, 0),
    mass : 1
  });
}

function distanceAttractiveForce(targetPos, forceOrigin, coeff, magFunction) {
  let directionVector =
    forceOrigin.subtract(targetPos).unit();

  let dist = forceOrigin.subtract(targetPos).length();

  let mag = magFunction(dist) * coeff;
  let forceVector = directionVector.multiply(mag);

  return forceVector;
}


function update(deltaTime) {
  particles = particles.filter(particle =>
    particle.location.X > -removeMargin && particle.location.X < app.bounds.X + removeMargin &&
    particle.location.Y > -removeMargin && particle.location.Y < app.bounds.Y + removeMargin);

  if(particles.length < maxParticleCount) {
    for (var i = 0; i < maxParticleCount - particles.length; i++) {
      //spawnParticle(true);
    }
  }

  particles.forEach((particle) => {
    let forces = [
      distanceAttractiveForce(
        particle.location, app.mousePos, 10000000, dist => -1 / dist)];

    for(i = 0; i < forces.length; i++) {
      particle.velocity = particle.velocity
        .add(forces[i].multiply(deltaTime).multiply(1 / particle.mass));
    }

    particle.velocity = particle.velocity.multiply(1 / friction);

    particle.location = particle.location.add(
      particle.velocity.multiply(deltaTime));
  });

  if(app.mouseDown) {
    if(particles.length > maxParticleCount) {
      particles.shift();
    }

    particles.push({
      location : app.mousePos,
      velocity : new Vec2(0, 0),
      mass : 1
    });
  }
}

function drawConnections(context) {
  particles.forEach((particle) => {



    particles.forEach(otherParticle => {
      let distance = otherParticle.location
        .subtract(particle.location).length();

      if (distance > lineDistance) {
        return;
      }



      context.beginPath();
      context.moveTo(particle.location.X, particle.location.Y);
      context.lineWidth = distance / lineDistance;

      context.lineTo(
        otherParticle.location.X, otherParticle.location.Y);
      context.closePath();
      context.stroke();
    });
  });


}

function draw(context) {
  drawConnections(context);

  particles.forEach((particle) => {
    context.beginPath();
    let radius = 4;


    context.ellipse(
      particle.location.X, particle.location.Y,
      radius, radius, 0, 0, 360);



    context.closePath();
    context.fill();
  });
}

function initParticles() {
  for (var i = 0; i < maxParticleCount; i++) {
    //spawnParticle(false);
  }
}

initParticles();

app.run();
