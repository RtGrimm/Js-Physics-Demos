



let renderTarget = document.querySelector("#RenderTarget");
let context = renderTarget.getContext("2d");

function Vec2(x, y) {
  this.X = x;
  this.Y = y;

  this.add = (other) => {
    return new Vec2(this.X + other.X, this.Y + other.Y);
  };

  this.subtract = (other) => {
    return new Vec2(this.X - other.X, this.Y - other.Y);
  };

  this.multiply = (other) => {
    return new Vec2(this.X * other, this.Y * other);
  };

  this.length = () => {
    return Math.sqrt(this.X * this.X + this.Y * this.Y);
  };

  this.unit = () => {
    let length = this.length();
    return new Vec2(this.X / length, this.Y / length);
  };

  this.abs = () => {
    return new Vec2(Math.abs(this.X), Math.abs(this.Y));
  };
}


let mousePos = new Vec2(0, 0);

renderTarget.addEventListener("mousemove", (e) => {
  mousePos = new Vec2(e.clientX, e.clientY);
});

function runLoop(callback) {
  window.requestAnimationFrame(() => {
    callback();
    runLoop(callback);
  });
}

let lastTime = Date.now();

let gravBodies = [
  {
    location : new Vec2(500, 500),
    mass : 100000000,
    velocity :  new Vec2(0, 0)
  },
  {
    location : new Vec2(300, 300),
    mass : 10000000,
    velocity : new Vec2(0.05, 0.01)
  },
  {
    location : new Vec2(400, 400),
    mass : 500000,
    velocity : new Vec2(0.05, 0.01)
  },
  {
    location : new Vec2(30, 120),
    mass : 500,
    velocity : new Vec2(0.05, 0.01)
  },
  {
    location : new Vec2(150, 40),
    mass : 1000,
    velocity : new Vec2(0.05, 0.01)
  },
  {
    location : new Vec2(100, 120),
    mass : 10000,
    velocity : new Vec2(0.05, 0.01)
  },
  {
    location : new Vec2(76, 40),
    mass : 10000,
    velocity : new Vec2(0.05, 0.01)
  }
];

function gravity(body1, body2) {
  let gravityConstant = 6.674 * Math.pow(10, -11);

  return (body2.location.subtract(body1.location).unit()).multiply(-gravityConstant * (
    (body1.mass * body2.mass) /
    body2.location.subtract(body1.location).length()))
}

function getForces(targetBody) {
  return gravBodies
    .filter(body => body != targetBody)
    .map(body => gravity(body, targetBody));
}

function update(deltaTime) {
  gravBodies.forEach((body) => {
    let forces = getForces(body);
    for(i = 0; i < forces.length; i++) {
      body.velocity = body.velocity
        .add(forces[i].multiply(deltaTime).multiply(1 / body.mass));
    }

    body.location = body.location.add(body.velocity.multiply(deltaTime));
  });
}

function draw() {

  gravBodies.forEach((body) => {
    context.beginPath();
    let radius = Math.log10(body.mass);


    context.ellipse(body.location.X, body.location.Y, radius, radius, 0, 0, 360);
    context.closePath();
    context.fill();
  });



}

runLoop(() => {
  let deltaTime = (Date.now() - lastTime) / 1000;

  //context.clearRect(0, 0, 1600, 1200);

  update(deltaTime);
  draw();


});
